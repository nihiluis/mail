package main

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"strings"

	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	echoMiddleware "github.com/labstack/echo/v4/middleware"
	log "github.com/sirupsen/logrus"
)

var validate *validator.Validate

type API struct {
	validate *validator.Validate
	config   *Config
}

type Config struct {
	Port         string
	Provider     string
	AllowOrigins []string
	SMTPConfig   *SMTPConfig
}

func NewService(config *Config) (*API, error) {
	validate := validator.New()

	return &API{validate, config}, nil
}

// AddHandlers adds the echo handlers that are part of this package.
func (api *API) AddHandlers(echo *echo.Echo) {
	echo.Use(echoMiddleware.Logger())

	echo.Use(echoMiddleware.CORSWithConfig(echoMiddleware.CORSConfig{
		AllowOrigins:     api.config.AllowOrigins,
		AllowCredentials: true,
		AllowMethods:     []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	echo.POST("/mail/send", api.handleMailSend)
}

type mailSendRequestBody struct {
	Recipient string `json:"recipient" validate:"required"`
	Subject   string `json:"subject" validate:"required"`
	Message   string `json:"message" validate:"required"`
}

func (api *API) handleMailSend(c echo.Context) error {
	body := new(mailSendRequestBody)
	if err := c.Bind(body); err != nil {
		return err
	}

	log.Debug(fmt.Sprintf("Sending mail to %s", body.Recipient))

	err := api.config.SMTPConfig.sendMailSMTPNoTLS(body.Recipient, body.Subject, body.Message)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, echo.Map{})
}

// SMTPConfig contains all data related to the Smtp instance from which a mail should be sent.
type SMTPConfig struct {
	User     string
	Password string
	Sender   string
	Addr     string
}

func (config *SMTPConfig) sendMailSMTPWithTLS(recipient string, subject string, message string) error {
	auth := sasl.NewPlainClient("", config.User, config.Password)

	to := []string{recipient}
	msg := strings.NewReader("To: " + recipient + "\r\n" +
		"Subject: Password recovery\r\n" +
		"\r\n" +
		message + "\r\n")

	err := smtp.SendMail(config.Addr, auth, config.Sender, to, msg)

	return err
}

func (config *SMTPConfig) sendMailSMTPNoTLS(recipient string, subject string, message string) error {
	auth := sasl.NewPlainClient("", config.User, config.Password)

	c, err := smtp.Dial(config.Addr)
	if err != nil {
		return err
	}

	if err := c.StartTLS(&tls.Config{
		// Set InsecureSkipVerify to skip the default validation we are
		// replacing. This will not disable VerifyConnection.
		InsecureSkipVerify: true,
	}); err != nil {
		return err
	}

	if err := c.Auth(auth); err != nil {
		return err
	}

	// Set the sender and recipient first
	if err := c.Mail(config.Sender, nil); err != nil {
		return err
	}
	if err := c.Rcpt(recipient); err != nil {
		return err
	}

	// Send the email body.
	wc, err := c.Data()
	if err != nil {
		return err
	}

	_, err = fmt.Fprintf(wc,
		"From: "+config.Sender+"\r\n"+
			"To: "+recipient+"\r\n"+
			"Subject: "+subject+"\r\n"+
			"\r\n"+
			message+"\r\n")
	if err != nil {
		return err
	}
	err = wc.Close()
	if err != nil {
		return err
	}

	// Send the QUIT command and close the connection.
	err = c.Quit()
	if err != nil {
		return err
	}

	return nil
}
