package main

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
)

func main() {
	err := godotenv.Load(".env")

	smtpConfig := SMTPConfig{
		User:     os.Getenv("MAIL_SMTP_USER"),
		Password: os.Getenv("MAIL_SMTP_PASSWORD"),
		Addr:     os.Getenv("MAIL_SMTP_ADDRESS"),
		Sender:   os.Getenv("MAIL_SENDER"),
	}

	config := &Config{
		Port:         os.Getenv("PORT"),
		Provider:     "",
		AllowOrigins: []string{},
		SMTPConfig:   &smtpConfig,
	}

	e := echo.New()

	api, err := NewService(config)
	if err != nil {
		panic(err)
	}
	api.AddHandlers(e)

	log.Info("Loaded all services")
	log.Info("HTTP server starting on")

	e.Logger.Fatal(e.Start(fmt.Sprintf(":%s", config.Port)))
}
